<?php

namespace Beyond\WeChatEcology\Payment\Partner\Ecommerce;

use Beyond\WeChatEcology\Payment\PaymentClient;

/**
 * 微信支付->服务商->收付通
 *
 * Class EcommerceClient
 * @package Beyond\WeChatEcology\Payment\Partner\Ecommerce
 */
abstract class EcommerceClient extends PaymentClient
{

}