<?php

namespace Beyond\WeChatEcology\Payment\Partner\Ecommerce;

use Beyond\SmartHttp\Kernel\Exceptions\AuthorizationException;
use Beyond\SmartHttp\Kernel\Exceptions\BadRequestException;
use Beyond\SmartHttp\Kernel\Exceptions\InvalidArgumentException;
use Beyond\SmartHttp\Kernel\Exceptions\ResourceNotFoundException;
use Beyond\SmartHttp\Kernel\Exceptions\ServiceInvalidException;
use Beyond\SmartHttp\Kernel\Exceptions\ValidationException;

/**
 * 微信支付->服务商->收付通->进件、分账、余额查询、商户提现
 *
 * Class Ecommerce
 * @package Beyond\WeChatEcology\Payment\Partner\Ecommerce
 */
class Ecommerce extends EcommerceClient
{
    /**
     * 二级商户进件
     *
     * @param $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function apply($params)
    {
        return $this->httpPostJson('/v3/ecommerce/applyments/', $params);
    }

    /**
     * 查询申请状态
     *
     * @param $id
     * @param string $type
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws InvalidArgumentException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function queryApplyState($id, $type = 'applyment')
    {
        if ('applyment' == $type) {
            $format = '/v3/ecommerce/applyments/%s';
        } elseif ('request' == $type) {
            $format = '/v3/ecommerce/applyments/out-request-no/%s';
        } else {
            throw new InvalidArgumentException('type参数错误。支持:applyment,request', 200001);
        }

        return $this->httpGet(sprintf($format, $id));
    }

    /**
     * 请求分账
     *
     * @param array $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function profitSharing(array $params)
    {
        return $this->httpPostJson('/v3/ecommerce/profitsharing/orders', $params);
    }

    /**
     * 查询分账结果
     *
     * @param array $query
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function queryProfitSharing(array $query)
    {
        return $this->httpGet('/v3/ecommerce/profitsharing/orders', $query);
    }

    /**
     * 完结分账
     *
     * @param $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function finishOrder($params)
    {
        return $this->httpPostJson('/v3/ecommerce/profitsharing/finish-order', $params);
    }

    /**
     * 查询订单剩余待分金额
     *
     * @param $transactionId
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function query($transactionId)
    {
        return $this->httpGet(sprintf('/v3/ecommerce/profitsharing/orders/%s/amounts', $transactionId));
    }

    /**
     * 添加分账接收方
     *
     * @param array $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function addReceiver(array $params)
    {
        return $this->httpPostJson('/v3/ecommerce/profitsharing/receivers/add', $params);
    }

    /**
     * 删除分账接收方
     *
     * @param array $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function deleteReceiver(array $params)
    {
        return $this->httpPostJson('/v3/ecommerce/profitsharing/receivers/delete', $params);
    }

    /**
     * 二级商户提现
     *
     * @param array $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function withdraw(array $params)
    {
        return $this->httpPostJson('/v3/ecommerce/fund/withdraw', $params);
    }

}