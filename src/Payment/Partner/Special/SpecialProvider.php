<?php
/**
 * @user: BEYOND 2023/1/17 17:49
 */

namespace Beyond\WeChatEcology\Payment\Partner\Special;


use Beyond\WeChatEcology\Core\BaseProvider;

class SpecialProvider extends BaseProvider
{
    /**
     * @return array|mixed
     */
    public function registerList()
    {
        return array_merge(parent::registerList(), [
            'special' => function ($app) {
                return new Special($app);
            },
        ]);
    }
}