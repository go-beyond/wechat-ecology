<?php
/**
 * @user: BEYOND 2023/1/17 17:37
 */

namespace Beyond\WeChatEcology\Payment\Partner\Special;


use Beyond\WeChatEcology\Payment\PaymentClient;

/**
 * Class SpecialClient
 * @package Beyond\WeChatEcology\Payment\Partner\Special
 */
abstract class SpecialClient extends PaymentClient
{

}