<?php
/**
 * @user: BEYOND 2023/1/17 17:36
 */

namespace Beyond\WeChatEcology\Payment\Partner\Special;


use Beyond\SmartHttp\Kernel\Exceptions\AuthorizationException;
use Beyond\SmartHttp\Kernel\Exceptions\BadRequestException;
use Beyond\SmartHttp\Kernel\Exceptions\ResourceNotFoundException;
use Beyond\SmartHttp\Kernel\Exceptions\ServiceInvalidException;
use Beyond\SmartHttp\Kernel\Exceptions\ValidationException;

/**
 * 微信支付->服务商->特约商户->进件、
 *
 * Class Special
 * @package Beyond\WeChatEcology\Payment\Partner\Special
 */
class Special extends SpecialClient
{
    /**
     * 特约商户-进件提交申请单
     *
     * @param $params
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function applyment($params)
    {
        return $this->httpPostJson('/v3/applyment4sub/applyment', $params);
    }

    /**
     * 特约商户-通过业务申请编号查询申请状态
     *
     * @param $businessCode
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function queryApplymentState($businessCode)
    {
        return $this->httpGet(sprintf('/v3/applyment4sub/applyment/business_code/%s', $businessCode));
    }

    /**
     * 特约商户-查询结算账户
     *
     * @param $subMchId
     *
     * @return array|string
     * @throws AuthorizationException
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     * @throws ServiceInvalidException
     * @throws ValidationException
     */
    public function query($subMchId)
    {
        return $this->httpGet(sprintf('/v3/apply4sub/sub_merchants/%s/settlement', $subMchId));
    }
}