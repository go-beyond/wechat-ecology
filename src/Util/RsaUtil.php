<?php
/**
 * @user: BEYOND 2023/2/14 14:19
 */

namespace Beyond\WeChatEcology\Util;

/**
 * 开发者应当使用 微信支付平台证书中的公钥，对上送的敏感信息进行加密
 *
 * Class RsaUtil
 * @package Beyond\WeChatEcology\Util
 */
class RsaUtil
{
    /**
     * @var null
     */
    private static $publicKey = null;

    private static $instance = [];

    private function __construct()
    {
    }

    /**
     * @param $publicKeyPath
     *
     * @return RsaUtil|mixed|null
     */
    public static function instance($publicKeyPath)
    {
        if (file_exists($publicKeyPath)) {
            self::$publicKey = file_get_contents($publicKeyPath);
        } else {
            throw new \InvalidArgumentException('Invalid public key path');
        }

        $instance = self::$instance[md5(self::$publicKey)] ?? null;
        if (!$instance instanceof self) {
            $instance = self::$instance[md5(self::$publicKey)] = new self;
        }

        return $instance;
    }

    /**
     * 数据加密
     *
     * @param $cleartext
     *
     * @return string
     * @see https://pay.weixin.qq.com/wiki/doc/apiv3_partner/apis/chapter7_1_1.shtml
     */
    public function rsaEncrypt($cleartext)
    {
        if (openssl_public_encrypt($cleartext, $encrypted, self::$publicKey, OPENSSL_PKCS1_OAEP_PADDING)) {
            $cipherText = base64_encode($encrypted);
        } else {
            throw new \RuntimeException('encrypt failed');
        }

        return $cipherText;
    }

}